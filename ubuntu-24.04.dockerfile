# syntax=docker/dockerfile:1.2

FROM docker.io/library/ubuntu:24.04 AS build
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		build-essential \
		cmake \
		ninja-build \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/


# Z3

FROM docker.io/alpine/git as z3-src
ARG z3_version=z3-4.12.5
RUN git clone https://github.com/Z3Prover/z3.git --branch ${z3_version} /z3 \
	&& git -C /z3 gc --aggressive
RUN \
	--mount=type=bind,target=/patch,source=patch \
	if [ -e /patch/z3/${z3_version}/ ] ; then \
		git -C /z3 apply /patch/z3/${z3_version}/*.patch \
	; fi

FROM build AS z3-build-deps
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		git \
		libgmp-dev \
		python3 \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/

FROM z3-build-deps AS z3-build
COPY --from=z3-src /z3 /z3
RUN mkdir -p /z3/build \
	&& cd /z3/build \
	&& cmake -GNinja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_EXE_LINKER_FLAGS=-fuse-ld=gold \
		-DCMAKE_INSTALL_PREFIX=/usr/local/ \
		-DZ3_USE_LIB_GMP=TRUE \
		-DZ3_LINK_TIME_OPTIMIZATION=TRUE \
		..
RUN ninja -C /z3/build/

FROM z3-build as z3-test
RUN ninja -C /z3/build/ test-z3
RUN cd /z3/build \
	&& ./test-z3 /a

FROM z3-build AS z3-package
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		file \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
RUN cd /z3/build \
	&& cpack -G DEB -P z3 -R 0.git-$(git rev-parse --short=16 HEAD) \
		-D CPACK_CMAKE_GENERATOR=Ninja \
		-D CPACK_PACKAGING_INSTALL_PREFIX=/usr/local \
		-D CPACK_INSTALL_CMAKE_PROJECTS='/z3/build;z3;ALL;/' \
		-D CPACK_PACKAGE_NAME='z3-git' \
		-D CPACK_PACKAGE_FILE_NAME='z3' \
		-D CPACK_PACKAGE_DESCRIPTION='The z3 constraint solver' \
		-D CPACK_PACKAGE_CONTACT=root \
		-D CPACK_DEBIAN_PACKAGE_SHLIBDEPS=ON
RUN true

FROM docker.io/library/ubuntu:24.04 as z3
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	--mount=type=bind,from=z3-package,target=/z3,source=/z3 \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		/z3/build/z3.deb \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
# docker buildx might leave these directories behind, but podman does not
RUN rmdir /z3 || true


# MINISAT

FROM docker.io/alpine/git AS minisat-src
RUN git clone https://github.com/stp/minisat.git /minisat \
	&& git -C /minisat gc --aggressive

FROM build AS minisat-build-deps
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		zlib1g-dev \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/

FROM minisat-build-deps AS minisat-build
COPY --from=minisat-src /minisat /minisat
RUN mkdir -p /minisat/build \
	&& cd /minisat/build \
	&& cmake -GNinja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE \
		-DCMAKE_EXE_LINKER_FLAGS=-fuse-ld=gold \
		-DCMAKE_INSTALL_PREFIX=/usr/local/ \
		..
RUN ninja -C /minisat/build/

FROM minisat-build AS minisat-package
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		file \
		git \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
RUN cd /minisat/build \
	&& cpack -G DEB -P minisat -R 0.git-$(git rev-parse --short=16 HEAD) \
		-D CPACK_CMAKE_GENERATOR=Ninja \
		-D CPACK_PACKAGING_INSTALL_PREFIX=/usr/local \
		-D CPACK_INSTALL_CMAKE_PROJECTS='/minisat/build;minisat;ALL;/' \
		-D CPACK_PACKAGE_NAME='minisat-git' \
		-D CPACK_PACKAGE_FILE_NAME='minisat' \
		-D CPACK_PACKAGE_DESCRIPTION='The minisat constraint solver' \
		-D CPACK_PACKAGE_CONTACT=root \
		-D CPACK_DEBIAN_PACKAGE_SHLIBDEPS=ON

FROM docker.io/library/ubuntu:24.04 as minisat
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	--mount=type=bind,from=minisat-package,target=/minisat,source=/minisat \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		/minisat/build/minisat.deb \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
# docker buildx might leave these directories behind, but podman does not
RUN rmdir /minisat || true


# CRYPTOMINISAT

FROM docker.io/alpine/git AS cryptominisat-src
ARG cryptominisat_version=5.11.15
RUN git clone https://github.com/msoos/cryptominisat /cryptominisat --branch ${cryptominisat_version} \
	&& git -C /cryptominisat gc --aggressive

FROM build AS cryptominisat-build-deps
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		libboost-program-options-dev \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/

FROM cryptominisat-build-deps AS cryptominisat-build
COPY --from=cryptominisat-src /cryptominisat /cryptominisat
RUN mkdir -p /cryptominisat/build \
	&& cd /cryptominisat/build \
	&& cmake -GNinja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE \
		-DCMAKE_EXE_LINKER_FLAGS=-fuse-ld=gold \
		-DCMAKE_INSTALL_PREFIX=/usr/local/ \
		..
RUN ninja -C /cryptominisat/build/

FROM cryptominisat-build AS cryptominisat-package
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		file \
		git \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
RUN cd /cryptominisat/build \
	&& cpack -G DEB -P cryptominisat -R 0.git-$(git rev-parse --short=16 HEAD) \
		-D CPACK_CMAKE_GENERATOR=Ninja \
		-D CPACK_PACKAGING_INSTALL_PREFIX=/usr/local \
		-D CPACK_INSTALL_CMAKE_PROJECTS='/cryptominisat/build;cryptominisat;ALL;/' \
		-D CPACK_PACKAGE_NAME='cryptominisat-git' \
		-D CPACK_PACKAGE_FILE_NAME='cryptominisat' \
		-D CPACK_PACKAGE_DESCRIPTION_SUMMARY='The cryptominisat constraint solver' \
		-D CPACK_PACKAGE_DESCRIPTION='The cryptominisat constraint solver' \
		-D CPACK_PACKAGE_CONTACT=root \
		-D CPACK_DEBIAN_PACKAGE_SHLIBDEPS=ON

FROM docker.io/library/ubuntu:24.04 as cryptominisat
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	--mount=type=bind,from=cryptominisat-package,target=/cryptominisat,source=/cryptominisat \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		/cryptominisat/build/cryptominisat.deb \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
# docker buildx might leave these directories behind, but podman does not
RUN rmdir /cryptominisat || true


# STP

FROM docker.io/alpine/git AS stp-src
ARG stp_version=master
RUN git clone https://github.com/stp/stp.git /stp --branch ${stp_version} \
	&& git -C /stp gc --aggressive

FROM build AS stp-build-deps
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	--mount=type=bind,from=minisat-package,target=/minisat,source=/minisat \
	--mount=type=bind,from=cryptominisat-package,target=/cryptominisat,source=/cryptominisat \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		cmake \
		bison \
		flex \
		libboost-all-dev \
		libgmp-dev \
		pkgconf \
		python-is-python3 \
		python3-setuptools \
		perl \
		zlib1g-dev \
		/minisat/build/minisat.deb \
		/cryptominisat/build/cryptominisat.deb \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
# docker buildx might leave these directories behind, but podman does not
RUN rmdir /minisat /cryptominisat || true

FROM stp-build-deps AS stp-build
COPY --from=stp-src /stp /stp
RUN mkdir -p /stp/build \
	&& cd /stp/build \
	&& cmake -GNinja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE \
		-DCMAKE_EXE_LINKER_FLAGS=-fuse-ld=gold \
		-DCMAKE_INSTALL_PREFIX=/usr/local/ \
		..
RUN ninja -C /stp/build/

FROM stp-build AS stp-package
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		file \
		git \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
RUN cd /stp/build \
	&& cpack -G DEB -P stp -R 0.git-$(git rev-parse --short=16 HEAD) \
		-D CPACK_CMAKE_GENERATOR=Ninja \
		-D CPACK_PACKAGING_INSTALL_PREFIX=/usr/local \
		-D CPACK_INSTALL_CMAKE_PROJECTS='/stp/build;stp;ALL;/' \
		-D CPACK_PACKAGE_NAME='stp-git' \
		-D CPACK_PACKAGE_FILE_NAME='stp' \
		-D CPACK_PACKAGE_DESCRIPTION='The STP constraint solver' \
		-D CPACK_PACKAGE_CONTACT=root \
		-D CPACK_DEBIAN_PACKAGE_SHLIBDEPS=ON

FROM docker.io/library/ubuntu:24.04 as stp
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	--mount=type=bind,from=minisat-package,target=/minisat,source=/minisat \
	--mount=type=bind,from=cryptominisat-package,target=/cryptominisat,source=/cryptominisat \
	--mount=type=bind,from=stp-package,target=/stp,source=/stp \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		/minisat/build/minisat.deb \
		/cryptominisat/build/cryptominisat.deb \
		/stp/build/stp.deb \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
# docker buildx might leave these directories behind, but podman does not
RUN rmdir /minisat /cryptominisat /stp || true


# KLEE-UCLIBC

FROM docker.io/alpine/git AS klee-uclibc-src
RUN git clone https://github.com/klee/klee-uclibc.git /klee-uclibc \
	&& git -C /klee-uclibc gc --aggressive

FROM build AS klee-uclibc-build-deps
ARG LLVM
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		clang-${LLVM} \
		curl \
		llvm-${LLVM}-dev \
		python-is-python3 \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/

FROM klee-uclibc-build-deps AS klee-uclibc-build
ARG LLVM
COPY --from=klee-uclibc-src /klee-uclibc /klee-uclibc
# uclibc does *not support* parallel builds
RUN cd /klee-uclibc \
	&& ./configure --with-llvm-config=$(which llvm-config-${LLVM}) --make-llvm-lib \
	&& make


# KLEE

FROM docker.io/alpine/git AS klee-src
RUN git clone https://github.com/klee/klee /klee \
	&& git -C /klee gc --aggressive

FROM build AS klee-build-deps
ARG LLVM
COPY --from=klee-uclibc-build /klee-uclibc /klee-uclibc
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		clang-${LLVM} \
		libcap-dev \
		libgoogle-perftools-dev \
		libgtest-dev \
		libsqlite3-dev \
		llvm-${LLVM}-dev \
		pipx \
		python3-tabulate \
		zlib1g-dev \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
RUN --mount=type=cache,sharing=locked,target=/root/.cache/pip/,id=ubuntu:24.04/root/.cache/pip/ \
	PIPX_HOME=/usr/local/pipx PIPX_BIN_DIR=/usr/local/bin pipx install lit
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	--mount=type=bind,from=z3-package,target=/z3,source=/z3 \
	--mount=type=bind,from=minisat-package,target=/minisat,source=/minisat \
	--mount=type=bind,from=cryptominisat-package,target=/cryptominisat,source=/cryptominisat \
	--mount=type=bind,from=stp-package,target=/stp,source=/stp \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		/z3/build/z3.deb \
		/minisat/build/minisat.deb \
		/cryptominisat/build/cryptominisat.deb \
		/stp/build/stp.deb \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
# docker buildx might leave these directories behind, but podman does not
RUN rmdir /z3 /minisat /cryptominisat /stp || true

FROM klee-build-deps AS klee-dev
ARG LLVM
RUN --mount=type=cache,sharing=locked,target=/root/.cache/pip/,id=ubuntu:24.04/root/.cache/pip/ \
	PIPX_HOME=/usr/local/pipx PIPX_BIN_DIR=/usr/local/bin pipx install wllvm
ENV LLVM_COMPILER=clang LLVM_COMPILER_PATH=/usr/lib/llvm-${LLVM}/bin/
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		bash-completion \
		clang-format \
		doxygen \
		file \
		gdb \
		git \
		graphviz \
		lldb \
		ssh \
		sudo \
		vim \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/

FROM klee-build-deps AS klee-build
ARG LLVM
COPY --from=klee-src /klee /klee
RUN mkdir -p /klee/build \
	&& cd /klee/build \
	&& cmake -GNinja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE \
		-DCMAKE_EXE_LINKER_FLAGS='-fuse-ld=gold -Wl,-L/usr/local/lib' \
		-DCMAKE_INSTALL_PREFIX=/usr/local/ \
		-DLLVM_DIR="$(llvm-config-${LLVM} --obj-root)" \
		-DKLEE_UCLIBC_PATH=/klee-uclibc \
		-DENABLE_POSIX_RUNTIME=TRUE \
		-DENABLE_UNIT_TESTS=ON \
		..
RUN ninja -C /klee/build/
RUN ninja -C /klee/build/ check

FROM klee-build AS klee-package
ARG LLVM
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		file \
		git \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
RUN cd /klee/build \
	&& cpack -G DEB -P klee -R 0.git-$(git rev-parse --short=16 HEAD) \
		-D CPACK_CMAKE_GENERATOR=Ninja \
		-D CPACK_PACKAGING_INSTALL_PREFIX=/usr/local \
		-D CPACK_INSTALL_CMAKE_PROJECTS='/klee/build;klee;ALL;/' \
		-D CPACK_PACKAGE_NAME='klee-git' \
		-D CPACK_PACKAGE_FILE_NAME='klee' \
		-D CPACK_PACKAGE_DESCRIPTION='The KLEE Symbolic Execution Engine' \
		-D CPACK_PACKAGE_CONTACT=root \
		-D CPACK_DEBIAN_PACKAGE_SHLIBDEPS=ON \
		-D CPACK_DEBIAN_PACKAGE_DEPENDS="python3, python3-tabulate, llvm-${LLVM}, z3-git, stp-git"


# COMBINE THE MAGIC!

FROM docker.io/library/ubuntu:24.04 as klee
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/apt/,id=ubuntu:24.04/var/cache/apt/ \
	--mount=type=cache,sharing=locked,target=/var/lib/apt/lists/,id=ubuntu:24.04/var/lib/apt/lists/ \
	--mount=type=bind,from=z3-package,target=/z3,source=/z3 \
	--mount=type=bind,from=minisat-package,target=/minisat,source=/minisat \
	--mount=type=bind,from=cryptominisat-package,target=/cryptominisat,source=/cryptominisat \
	--mount=type=bind,from=stp-package,target=/stp,source=/stp \
	--mount=type=bind,from=klee-package,target=/klee,source=/klee \
	mv /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes / \
	&& apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
		/z3/build/z3.deb \
		/minisat/build/minisat.deb \
		/cryptominisat/build/cryptominisat.deb \
		/stp/build/stp.deb \
		/klee/build/klee.deb \
	&& mv /docker-clean /docker-gzip-indexes /etc/apt/apt.conf.d/
# docker buildx might leave these directories behind, but podman does not
RUN rmdir /z3 /minisat /cryptominisat /stp /klee || true
