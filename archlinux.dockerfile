# syntax=docker.io/docker/dockerfile:1.4

FROM docker.io/library/archlinux AS build
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/pacman/pkg/,id=archlinux/var/cache/pacman/pkg/ \
	--mount=type=cache,sharing=locked,target=/var/lib/pacman/sync/,id=archlinux/var/lib/pacman/sync/ \
	pacman --noconfirm -Syu \
		base-devel \
		cmake \
		cryptominisat \
		gperftools \
		gtest \
		libcap \
		minisat \
		mold \
		ninja \
		python \
		python-pipx \
		python-psutil \
		python-tabulate \
		sqlite \
		stp \
		z3 \
		zlib


# LLVM

FROM docker.io/alpine/git AS llvm-src
ARG LLVM=14.0.6
ARG LLVM_BRANCH=llvmorg-${LLVM}
RUN git clone https://github.com/llvm/llvm-project /llvm-src --depth 1 --branch ${LLVM_BRANCH} \
	&& git -C /llvm-src gc --aggressive
RUN \
	--mount=type=bind,target=/patch,source=patch \
	if [ -e /patch/llvm/${LLVM}/ ] ; then \
		git -C /llvm-src apply /patch/llvm/${LLVM}/*.patch \
	; fi

FROM build AS llvm-build
ARG LLVM=14.0.6
RUN \
	--mount=type=bind,from=llvm-src,source=/llvm-src,target=/llvm-src \
	mkdir /llvm-build && \
	cd /llvm-build && \
	cmake -GNinja \
		-DLLVM_ENABLE_PROJECTS=clang \
		-DLLVM_TARGETS_TO_BUILD=X86 \
		-DLLVM_INSTALL_UTILS=On \
		-DCLANG_ENABLE_STATIC_ANALYZER=Off \
		-DCLANG_ENABLE_ARCMT=Off \
		-DCMAKE_BUILD_TYPE=Release \
		-DLLVM_USE_LINKER=mold \
		-DLLVM_ENABLE_BINDINGS=OFF \
		-DCMAKE_INSTALL_PREFIX=/usr/local/llvm-${LLVM} \
		/llvm-src/llvm && \
	ninja
	# ninja check-all

FROM build AS llvm
ARG LLVM=14.0.6
RUN \
	--mount=type=bind,from=llvm-src,source=/llvm-src,target=/llvm-src \
	--mount=type=bind,from=llvm-build,source=/llvm-build,target=/llvm-build,rw \
	ninja -C /llvm-build install && \
	PIPX_HOME=/usr/local/pipx PIPX_BIN_DIR=/usr/local/bin pipx install wllvm
ENV \
	PATH=$PATH:/usr/local/llvm-${LLVM}/bin \
	LLVM_COMPILER=clang LLVM_COMPILER_PATH=/usr/local/llvm-${LLVM}/bin


# KLEE-UCLIBC

FROM docker.io/alpine/git AS klee-uclibc-src
RUN git clone https://github.com/klee/klee-uclibc.git /klee-uclibc \
	&& git -C /klee-uclibc gc --aggressive

FROM llvm AS klee-uclibc-build-deps
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/pacman/pkg/,id=archlinux/var/cache/pacman/pkg/ \
	--mount=type=cache,sharing=locked,target=/var/lib/pacman/sync/,id=archlinux/var/lib/pacman/sync/ \
	pacman --noconfirm -Syu \
		curl

FROM klee-uclibc-build-deps AS klee-uclibc-build
COPY --from=klee-uclibc-src /klee-uclibc /klee-uclibc
# uclibc does *not support* parallel builds
RUN \
	cd /klee-uclibc && \
	./configure "--with-llvm-config=$(which llvm-config)" --make-llvm-lib && \
	make


# KLEE

FROM docker.io/alpine/git AS klee-src
RUN git clone https://github.com/klee/klee /klee-src \
	&& git -C /klee-src gc --aggressive

FROM llvm AS klee-build-deps
COPY --from=klee-uclibc-build /klee-uclibc /klee-uclibc
RUN --mount=type=cache,sharing=locked,target=/root/.cache/pip/,id=debian:testing/root/.cache/pip/ \
	PIPX_HOME=/usr/local/pipx PIPX_BIN_DIR=/usr/local/bin pipx install lit

FROM klee-build-deps AS klee-dev
RUN \
	--mount=type=cache,sharing=locked,target=/var/cache/pacman/pkg/,id=archlinux/var/cache/pacman/pkg/ \
	--mount=type=cache,sharing=locked,target=/var/lib/pacman/sync/,id=archlinux/var/lib/pacman/sync/ \
	pacman --noconfirm -Syu \
		bash-completion \
		doxygen \
		file \
		gdb \
		git \
		graphviz \
		lldb

FROM klee-build-deps AS klee-build
# note: rw necessary for klee test only, as `klee-stats` currently requires write access to source folder during 2 tests
RUN \
	--mount=type=bind,from=klee-src,source=/klee-src/,target=/klee-src/,rw \
	mkdir -p /klee-build && \
	cd /klee-build && \
	cmake -GNinja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE \
		-DCMAKE_EXE_LINKER_FLAGS='-fuse-ld=mold -Wl,-L/usr/local/lib' \
		-DCMAKE_INSTALL_PREFIX=/usr/local/ \
		"-DLLVM_DIR=$(llvm-config --obj-root)" \
		-DKLEE_UCLIBC_PATH=/klee-uclibc \
		-DENABLE_POSIX_RUNTIME=TRUE \
		-DENABLE_UNIT_TESTS=ON \
		/klee-src && \
	ninja && \
	ninja check

FROM llvm AS klee
RUN \
	--mount=type=bind,from=klee-src,source=/klee-src/,target=/klee-src/ \
	--mount=type=bind,from=klee-build,source=/klee-build/,target=/klee-build/,rw \
	ninja -C klee-build install
